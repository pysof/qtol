A pysof implementation of _jake_, from the Unified Theory of Language

# Installation Instructions

```bash
$ curl -L https://pysof.org | bash
```

[pysof.org](http://pysof.org)

[re:is](https://gitlab.com/pysof/qtol/tree/master/instruction_sets)

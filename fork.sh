#!/bin/bash

#
# Welcome to pysof.org
#  Confused? Start here: https://gitlab.com/pysof/qtol
#
# requirements: bash, curl, git
#     optional: python3
#
# bootstrapping:
#   $ curl https://pysof.org | bash
#

export set PYSOF=${PYSOF:=jk}
export set PYSOF_BRANCH=${PYSOF_BRANCH:=master}
export set PYSOF_REPO=${PYSOF_REPO:=pysof/qtol}
export set GITLAB=${GITLAB:=gitlab.com}

curl https://${GITLAB}/${PYSOF_REPO}/raw/${PYSOF_BRANCH}/.b/fork.sh | bash

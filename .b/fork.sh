#!/bin/bash

export set PYSOF=${PYSOF:=jk}
export set PYSOF_BRANCH=${PYSOF_BRANCH:=master}
export set PYSOF_REPO=${PYSOF_REPO:=pysof/qtol}
export set GITLAB=${GITLAB:=gitlab.com}

echo "Initial Unimplementation of Jake (${PYSOF})"
echo "Repository: ${PYSOF_REPO}"
echo "Branch: ${PYSOF_BRANCH}"
echo "Host: ${GITLAB}"

exit 255
